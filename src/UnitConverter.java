/**
 * Convert the value from one unit to another unit.
 * @author Supanat Pokturng
 * @version 2015.03.03
 */
public class UnitConverter {
	
	/**
	 * Use to convert value from one unit to another unit.
	 * @param amount is value use to convert
	 * @param fromUnit is unit that use to convert
	 * @param toUnit is unit that converted
	 * @return value that converted
	 */
	public double convert( double amount , Unit fromUnit , Unit toUnit ) {
		return fromUnit.convertTo(toUnit , amount );
	}
	
	/**
	 * Get unit in Length class.
	 * @return array of units
	 */
	public Unit[] getUnits() {
		return Length.values();
	}
}
