import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.zip.DataFormatException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Graphics User Interface of the converter program.
 * convert amount from ont unit to another unit
 * @author Supanat Pokturng
 * @version 2015.03.03
 */
public class ConverterUI extends JFrame {
	
	/** Declare the attributes. */
	private JTextField txt_fromUnit;
	private JComboBox cb_fromUnit;
	private JLabel lb_equals;
	private JTextField txt_toUnit;
	private JComboBox cb_toUnit;
	private JButton btn_convert;
	private JButton btn_clear;
	private UnitConverter unitConverter;
	
	/**
	 * Constructor of the class.
	 * @param unitConverter is an unitConverter that use to convert
	 */
	public ConverterUI( UnitConverter unitConverter) {
		this.unitConverter = unitConverter;
		initComponents();
	}
	
	/**
	 * Interface of the program.
	 * Can convert the value from one unit to another unit
	 */
	public void initComponents() {
		
		/** Close and stop program when click close. */
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/** Create a title of the window. */
		setTitle( "Distance Converter" );
		
		/** Declare and initialize the array of units. */
		Unit[] units = Length.values();
		
		txt_fromUnit = new JTextField(15);
		
		/**
		 * Calculate to convert by press Enter.
		 */
		txt_fromUnit.addActionListener( new ActionListener() {
			public void actionPerformed( ActionEvent e ) {
				try{
					double amount = Double.parseDouble(txt_fromUnit.getText());
					if( amount < 0 )
						throw new DataFormatException();
					Length fromUnit = Length.valueOf( (cb_fromUnit.getSelectedItem()+"").toUpperCase() );
					Length toUnit = Length.valueOf( (cb_toUnit.getSelectedItem()+"").toUpperCase() );
					txt_toUnit.setText(unitConverter.convert(amount, fromUnit, toUnit)+"");
				}catch( NumberFormatException ex ){
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Number");
				}catch( DataFormatException ex ){
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Positive Number");
				}
			}
		});
		
		cb_fromUnit = new JComboBox<Unit>(units);
		lb_equals = new JLabel(" = ");
		txt_toUnit = new JTextField(15);
		txt_toUnit.setEditable(false);
		txt_toUnit.setBackground(Color.WHITE);
		cb_toUnit = new JComboBox<Unit>(units);
		btn_convert = new JButton("Convert!");
		
		/**
		 * Calculate function to convert.
		 */
		btn_convert.addActionListener( new ActionListener() {
			public void actionPerformed( ActionEvent e ) {
				try{
					double amount = Double.parseDouble(txt_fromUnit.getText());
					if( amount < 0 )
						throw new DataFormatException();
					Length fromUnit = Length.valueOf( (cb_fromUnit.getSelectedItem()+"").toUpperCase() );
					Length toUnit = Length.valueOf( (cb_toUnit.getSelectedItem()+"").toUpperCase() );
					txt_toUnit.setText(unitConverter.convert(amount, fromUnit, toUnit)+"");
				}catch( NumberFormatException ex ){
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Number");
				}catch( DataFormatException ex ){
					JOptionPane.showMessageDialog(ConverterUI.this , "Please Input a Positive Number");
				}
			}
		});
		
		btn_clear = new JButton("Clear");
		
		/**
		 * Clear the numbers in textFields.
		 */
		btn_clear.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_fromUnit.setText("");
				txt_toUnit.setText("");	
			}
		});
		
		/**
		 * Add the components to the container.
		 */
		Container container = super.getContentPane();
		container.setLayout( new FlowLayout());
		container.add(txt_fromUnit);
		container.add(cb_fromUnit);
		container.add(lb_equals);
		container.add(txt_toUnit);
		container.add(cb_toUnit);
		container.add(btn_convert);
		container.add(btn_clear);
		pack();
		super.setVisible(true);
		
	}
}
