/**
 * Application to execute the program.
 * @author Supanat Pokturng
 * @version 2015.03.03
 */
public class Main {
	
	/**
	 * Execute the program.
	 * @param args is arguments
	 */
	public static void main(String [] args) {
		UnitConverter unitConverter = new UnitConverter();
		ConverterUI convertUI = new ConverterUI(unitConverter);
	}
}
