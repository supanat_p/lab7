/**
 * Use to keep unit in the class.
 * and has the value to calculate
 * @author Supanat Pokturng
 * @version 2015.03.03
 */
public enum Length implements Unit {
	/** Initialize the unit and value. */
	METER( "meter" , 1.0 ),
	CENTIMETER( "centimeter" , 0.01 ),
	KILOMETER( "kilometer" , 1000.0 ),
	MILE( "mile" , 1609.344 ),
	FOOT( "foot" , 0.30480 ),
	WA( "waaa" , 2.0 );
	
	/** Declare the attribute. */
	private String name;
	private double value;
	
	/**
	 * Constructor of the class that initialize the attributes.
	 * @param name is name of the unit
	 * @param value is amount of the unit to convert to meter
	 */
	Length( String name , double value ) {
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Convert that amount of this unit to another unit.
	 * @param unit is the unit before convert
	 * @param amount is value that convert
	 * @return amount that converted
	 */
	public double convertTo( Unit unit , double amount ) {
		return amount * this.value / unit.getValue();
	}
	
	/**
	 * Get a value to convert to meter.
	 * @return value of this unit to convert to meter
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get a description.
	 * @return a String of name of this unit
	 */
	public String toString() {
		return name;
	}
}
