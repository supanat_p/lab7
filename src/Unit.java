/**
 * Unit can convert value from one unit to another unit.
 * 
 * @author Supanat Pokturng
 * @version 2015.03.03
 */
public interface Unit {
	
	/**
	 * Convert value from one unit to another unit.
	 * @param unit before convert
	 * @param amount is value that convert
	 * @return value of converted unit
	 */
	public double convertTo( Unit unit , double amount );
	
	/**
	 * Get a value of this unit.
	 * @return value of Unit
	 */
	public double getValue();	
	
	/**
	 * Get a description of this Unit.
	 * @return a detail of unit
	 */
	public String toString();
}
